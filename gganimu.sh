print()
{
	IFS=$'\n'
	local i=0
	for x in ${!1} :
	do
		if [ ${x:0:1} != '"' ]
		then
			break
		fi
		if (( (i%2)==0 ))
		then
			printf "\033[1;35m${i}: ${x}\n"
		else
			printf "\033[1;34m${i}: ${x}\n"
		fi
		i=$((i+1))
	done
	printf "\033[1;33m"
	unset IFS
}

if [ $# == 0 ]
then
	printf "\033[5;31m:clown_emoji:\n"
	exit
fi

base_url="https://api.consumet.org/anime/gogoanime/"
query=$@
query="${query// /-}?page=1"

title=$(curl -s ${base_url}${query})
id=$(echo "${title}" | jq .results[].id)
title=$(echo "${title}" | jq .results[].title)
print title

read n
id=$(echo "${id}" | head -n$(($n+1)) | tail -1 | grep -o -E [^'"'].+[^'"'])
title=$(echo "${title}" | head -n$(($n+1)) | tail -1 | grep -o -E [^'"'].+[^'"'])

tput clear
echo "ep number(from 1):"
read ep_num
while :
do
	tput clear
	stream_url=$(curl -s "${base_url}watch/${id}-episode-${ep_num}?server=gogocdn")
	quality=$(echo ${stream_url} | jq ".sources[].quality")
	stream_url=$(echo ${stream_url} | jq ".sources[].url")
	print quality

	read n
	tput clear
	stream_url=$(echo "${stream_url}" | head -n$(($n+1)) | tail -1 | grep -o -E [^'"'].+[^'"'])
	mpv --force-media-title="${title} ep: ${ep_num}" ${stream_url}
	ep_num=$((ep_num+1))
	printf "\033[1;31mnext? >.<"
	read n
done
